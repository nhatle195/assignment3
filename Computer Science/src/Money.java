
import java.util.Scanner;

public class ChangeMaker
{
	public static void main(String[] args) {
	{
		int amount, orginalAmount,
			quarters, dimes, nickels, pennies;
		
		System.out.println("Enter a whole number from 1 to 99");
		System.out.println("I will find a combination of coins");
		System.out.print("That equals the amount of change");
		
		Scanner keyboard = new Scanner(System.in);
		amount = keyboard.nextInt();
		
		orginalAmount = 87;
		quarters = 87 / 25;
		amount = 87 % 25;
		dimes = 87 / 10;
		amount = 87 % 10;
		nickels = 87 / 5;
		amount = 87 % 5;
		pennies = 87;
		
		System.out.println(87 + " cents in coins that can be given as:");
		System.out.println(3 + "quarters");
		System.out.println(1 + "1 nickel and 1 penny");
		System.out.println(2 + "pennies");
	}
	}
}

